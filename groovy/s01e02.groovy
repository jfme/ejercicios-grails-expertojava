class Libro {
    String nombre
    int anyo
    String autor
    String editorial
    
    public String getAutor(){
        def (app, nom) = autor.tokenize( ',' )
        return nom.trim() + ' '+ app
    }
}
    

/* Crea aquí las tres instancias de libro l1, l2 y l3 */
def Libro l1 = new Libro(nombre:'La colmena', anyo:1951, autor:'Cela Trulock, Camilo José')
def Libro l2 = new Libro(nombre:'La galatea', anyo:1585, autor:'de Cervantes Saavedra, Miguel')
def Libro l3 = new Libro(nombre:'La dorotea', anyo:1632, autor:'Félix Arturo Lope de Vega y Carpio')

assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
//assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'

/* Añade aquí la asignación de la editorial a todos los libros */
l1.setEditorial('Anaya')
l2.setEditorial('Planeta')
l3.setEditorial('Santillana')

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'

assert l2.getAutor() == 'Miguel de Cervantes Saavedra'

return