def factorial = { n -> (n == 1) ? 1 : n * call(n - 1) }

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */
def listaEnteros = [2,4,7,10,12,16,27]
listaEnteros.each(){ entero ->
    println(factorial(entero))
}

def ayer     = { fecha -> (new Date().parse("dd/MM/yyyy HH:mm:s",fecha)-1).format('dd/MM/yyyy HH:mm:ss') }
def manana   = { fecha -> (new Date().parse("dd/MM/yyyy HH:mm:s",fecha)+1).format('dd/MM/yyyy HH:mm:ss') }

def listaFechas = ["21/10/2015 20:23:00","25/12/2016 23:00:00","31/12/1999 23:00:01"]

listaFechas.each(){ itemFecha ->
   println ('=== Hoy es: ' + itemFecha)
   println('    Ayer = ' + ayer(itemFecha))
   println('    Mañana = ' + manana(itemFecha))
}