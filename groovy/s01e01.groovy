class Todo{
    String titulo
    String descripcion
}


def todos = []
todos.add(new Todo(titulo:"lavadora", descripcion:"poner lavadora"))
todos.add(new Todo(titulo:"Impresora", descripcion:"Comprar cartuchos impresora"))
todos.add(new Todo(titulo:"Peliculas", descripcion:"Devolver peliculas videoclub"))

todos.each{item ->
    println(item.titulo + " " + item.descripcion)
}
