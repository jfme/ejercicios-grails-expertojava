class Calculadora {

    def valor1
    def valor2
    
    def public resta(int valor1, int valor2){
        return valor1 - valor2
    }
    
    def public suma(Integer valor1, Integer valor2){
        return valor1 + valor2
    }
    
    def public multiplicacion(valor1, valor2){
        return valor1 * valor2
    }
    
    def public division(valor1, valor2){
        return valor1/valor2
    }

}

def op1
def op2
def operacion

Calculadora calculadora = new Calculadora()

System.in.withReader{
    print 'Introduzca operacion a realizar (suma, resta, division, multiplicacion):'
    operacion = it.readLine()
    print 'Introduzca operador 1:'
    op1 =  new Integer(it.readLine())
    print 'Introduzca operador 2:'
    op2 = new Integer(it.readLine())

}

switch ( operacion ) {
    case "suma": println(calculadora.suma(op1, op2))
        break
    case "resta": println(calculadora.resta(op1, op2))
        break
    case "multiplicacion": println(calculadora.multiplicacion(op1, op2))
        break
    case "division": println(calculadora.division(op1, op2))
        break
    default: println("Operacion no permitida")
        break
}
