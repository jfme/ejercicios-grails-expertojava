package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    /*void "El método getNextTodos devuelve los siguientes todos de los días pasado por parámetro"() {
        given:
        def user = new User(username:"usuario2", password:"usuario2", name:"Paco", surnames:"Mel", email:"mail3@mmail.es")
        def todoDayBeforeYesterday = new Todo(title:"Todo day before yesterday", date: new Date() - 2, user: user)
        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1, user: user)
        def todoToday = new Todo(title:"Todo today", date: new Date(), user: user)
        def todoTomorrow = new Todo(title:"Todo tomorrow", date: new Date() + 1, user: user)
        def todoDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 2, user: user)
        def todoDayAfterDayAfterTomorrow = new Todo(title:"Todo after day after tomorrow", date: new Date() + 3, user: user)
        and:
        mockDomain(Todo,[todoDayBeforeYesterday, todoYesterday, todoToday, todoTomorrow, todoDayAfterTomorrow, todoDayAfterDayAfterTomorrow])
        and:
        def nextTodos = service.getNextTodos(2,[:])
        expect:
        Todo.count() == 6
        and:
        nextTodos.containsAll([todoTomorrow, todoDayAfterTomorrow])
        nextTodos.size() == 2
        and:
        !nextTodos.contains(todoDayBeforeYesterday)
        !nextTodos.contains(todoToday)
        !nextTodos.contains(todoYesterday)
        !nextTodos.contains(todoDayAfterDayAfterTomorrow)
    }*/

    void "El método saveTodo almacena el todo y añade la fecha de realización"() {
        given:
        def user = new User(username: "usuario2", password: "usuario2", name: "Paco", surnames: "Mel", email: "mail3@mmail.es")
        def todoToday = new Todo(title:"Todo today", date: new Date(), user: user, realizada: true)
        and:
        mockDomain(Todo,[todoToday])
        and:
        def todo = service.saveTodo(todoToday)
        expect:
        Todo.count() == 1
        and:
        todo.dateDone != null
        todo.dateDone.before(new Date())
    }
}
