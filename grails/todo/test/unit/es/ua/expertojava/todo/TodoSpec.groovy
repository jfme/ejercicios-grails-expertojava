package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "El atributo reminderDate no puede ser posterior a date de la tarea"() {

        given:
        def t1 = new Todo(reminderDate:new Date()+1, date:new Date())
        when:
        t1.validate()
        then:
        t1?.errors['reminderDate']
    }

    def "El atributo reminderDate puede ser anterior a date de la tarea"() {

        given:
        def t1 = new Todo(reminderDate:new Date(), date:new Date()+1)
        when:
        t1.validate()
        then:
        !t1?.errors['reminderDate']
    }

    def "El atributo reminderDate no puede ser igual a date de la tarea"() {

        given:
        def t1 = new Todo(reminderDate:new Date(), date:new Date())
        when:
        t1.validate()
        then:
        t1?.errors['reminderDate']
    }
}