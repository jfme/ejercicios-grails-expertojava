import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->

        def roleAdministrador = new Role(authority:"ROLE_ADMIN").save()
        def roleBasico = new Role(authority:"ROLE_BASIC").save()

        def userAdmin = new User(username:"admin", password:"admin", name:"Jose", surnames:"Muñoz", email:"mail@mail.es").save()
        def userBasic1 = new User(username:"usuario1", password:"usuario1", name:"Ana", surnames:"Botica", email:"mail2@mail.es").save()
        def userBasic2 = new User(username:"usuario2", password:"usuario2", name:"Juana", surnames:"Mel", email:"mail3@mail.es").save()

        PersonRole.create(userAdmin, roleAdministrador)
        PersonRole.create(userBasic1, roleBasico)
        PersonRole.create(userBasic2, roleBasico)


        def categoryHome = new Category(name:"Hogar").save()
        def categoryJob = new Category(name:"Trabajo").save()

        def tagEasy = new Tag(name:"Fácil").save()
        def tagDifficult = new Tag(name:"Difícil").save()
        def tagArt = new Tag(name:"Arte").save()
        def tagRoutine = new Tag(name:"Rutina").save()
        def tagKitchen = new Tag(name:"Cocina").save()

        def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date()+1, realizada: false)
        def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date()+2, realizada: false)
        def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date()+4, realizada: false)
        def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(), realizada: true)

        todoPaintKitchen.addToTags(tagDifficult)
        todoPaintKitchen.addToTags(tagArt)
        todoPaintKitchen.addToTags(tagKitchen)
        todoPaintKitchen.category = categoryHome
        todoPaintKitchen.save()

        todoCollectPost.addToTags(tagRoutine)
        todoCollectPost.category = categoryJob
        todoCollectPost.save()

        todoBakeCake.addToTags(tagEasy)
        todoBakeCake.addToTags(tagKitchen)
        todoBakeCake.category = categoryHome
        todoBakeCake.save()

        todoWriteUnitTests.addToTags(tagEasy)
        todoWriteUnitTests.category = categoryJob
        todoWriteUnitTests.save()

        /*for (String url in [
                '/', '/index', '/index.gsp', '*//**//*favicon.ico',
                '/assets*//**', '*//**//*js*//**', '*//**//*css*//**', '*//**//*images*//**',
         '/login', '/login.*', '/login*//*',
                '/logout', '/logout.*', '/logout*//*']) {
            new Requestmap(url: url, configAttribute: 'permitAll').save()
        }
        new Requestmap(url: '/profile*//**',    configAttribute: 'ROLE_USER').save()
         new Requestmap(url: '/admin*//**',      configAttribute: 'ROLE_ADMIN').save()
         new Requestmap(url: '/admin/role*//**', configAttribute: 'ROLE_SUPERVISOR').save()
         new Requestmap(url: '/admin/user*//**', configAttribute: 'ROLE_ADMIN,ROLE_SUPERVISOR').save()
         new Requestmap(url: '/j_spring_security_switch_user',
         configAttribute: 'ROLE_SWITCH_USER,isFullyAuthenticated()').save()*/
    }
    def destroy = { }
}
