package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "todo"

    def printIconFromBoolean = {attrs, body ->
        def mkp = new groovy.xml.MarkupBuilder(out)

        //def imagen = "${resource(dir: 'images', file: 'ko.png')}"
        //if(attrs['value'].equals(true)){
        //    imagen = "${resource(dir: 'images', file: 'ok.png')}"
        //}
        //mkp.img(src:imagen, body())

        def imagen = "ko.png"
        if(attrs['value'].equals(true)){
            imagen = "ok.png"
        }
        out<<asset.image(src:imagen, width: '20px')

        //<img src="${resource(dir: 'images', file: 'jose.jpg')}" height="250px" alt="Grails"/>
    }
}
