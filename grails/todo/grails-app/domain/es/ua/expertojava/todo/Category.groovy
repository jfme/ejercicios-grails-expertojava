package es.ua.expertojava.todo

class Category {

    String name
    String description

    static hasMany = [todos:Todo]

    static constraints = {
        name(nullable: false, blank:false, minSize: 1)
        description(blank:true, nullable:true, maxSize:1000)
    }

    String toString(){
        name
    }
}
