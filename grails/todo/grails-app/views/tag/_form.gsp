<%@ page import="es.ua.expertojava.todo.Tag" %>



<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="tag.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${tagInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'color', 'error')} ">
	<label for="color">
		<g:message code="tag.color.label" default="Color" />
		
	</label>
	<g:textField name="color" pattern="${tagInstance.constraints.color.matches}" value="${tagInstance?.color}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'todos', 'error')} ">
	<label for="todos">
		<g:message code="tag.todos.label" default="Todos" />
	</label>
	<g:select name="todos" from="${es.ua.expertojava.todo.Todo.list()}" multiple="multiple" optionKey="id" size="5" value="${tagInstance?.todos*.id}" class="many-to-many"/>

</div>

