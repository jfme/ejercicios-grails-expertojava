
<div id="header">
    <div id="menu">
        <nobr>
            <sec:ifNotLoggedIn>
                <g:link controller='login' action='auth'>
                    Login
                </g:link>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <g:form controller="logout" action="index" method="POST">
                    Username: <sec:username />
                    <g:submitButton name="logout" value="Logout" />
                </g:form>
            </sec:ifLoggedIn>
        </nobr>
    </div>
</div>