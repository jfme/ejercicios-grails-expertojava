package es.ua.expertojava.todo

import grails.transaction.Transactional

@Transactional
class CategoryService {

    def eliminarCategoria(Category categoryInstance) {
        //Desenlazamos las tareas que hacen referencia a la categoria
        List<Todo> listaTareas = Todo.findAllByCategory(categoryInstance)

        listaTareas.each {
            it.category = null
            it.save flush:true
        }

        //Eliminamos la categoria
        categoryInstance.delete flush:true
    }
}
