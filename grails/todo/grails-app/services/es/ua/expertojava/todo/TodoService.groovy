package es.ua.expertojava.todo

import grails.transaction.Transactional
import org.apache.tomcat.jni.Time

@Transactional
class TodoService {

    def eliminarTarea(Todo todoInstance) {
        if (todoInstance == null) {
            return
        }
        //Desenlazamos las etiquetas que hacen referencia a la categoria
        if (todoInstance.tags != null) {
            List<Tag> listaTags = new ArrayList<Tag>()
            listaTags.addAll(todoInstance.tags)

            listaTags.each {
                todoInstance.removeFromTags(it)
            }
        }

        todoInstance.delete flush:true
    }

    def tareasPorCategoria(List<Category> categorias) {
        List<Todo> listaTodos = Todo.findAllByCategoryInList(categorias, [sort:"date", order:'desc'])
    }

    def countTareasPorCategoria(List<Category> categorias) {
        Integer contador = 0
        if (categorias != null || categorias.size() > 0) {
            contador = Todo.countByCategoryInList(categorias)
        }
    }

    def saveTodo(Todo todoInstance) {
        if (todoInstance.getRealizada()) {
            todoInstance.setDateDone(new Date())
        }
        todoInstance.save flush:true
    }

    def lastTodosDone(Integer hours, params) {
        if (hours == null) {
            hours = 0
        }
        Date now = new Date(System.currentTimeMillis())
        Date to = Time.now() + hours

        Todo.findAllByDateDoneBetween(to, now, params)
    }

    def countLastTodosDone(Integer hours) {
        if (hours == null) {
            hours = 0
        }

        Date now = new Date(System.currentTimeMillis())
        Date to = Time.now() + hours

        Todo.countByDateDoneBetween(to, now)
    }

    def getNextTodos(Integer days, params) {
        if (days == null) {
            days = 0
        }

        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        List<Todo> listaTodos = Todo.findAllByDateBetween(now, to, params)
        Todo.findAllByDateBetween(now, to, params)
    }

    def countNextTodos(Integer days) {
        if (days == null) {
            days = 0
        }

        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }
}
